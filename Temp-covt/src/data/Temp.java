package data;

public class Temp {
	private String F,C=null;

	public String getF() {
		return F;
	}

	public void setF(String f) {
		F = f;
	}

	public String getC() {
		return C;
	}

	public void setC(String c) {
		C = c;
	}

	public Temp(String f, String c) {
		super();
		F = f;
		C = c;
	}
	
	
}
