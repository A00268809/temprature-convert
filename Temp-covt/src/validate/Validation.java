package validate;

import data.Temp;

public class Validation {

	public String validateCelsius(Temp t) {
		// TODO Auto-generated method stub
		String c = t.getC();
		
		if(c.equals("")) 
			return "Celsius must not be empty";
		if(c == null)
			return "Celsius must not be empty";
		if(!isNumbric(c))
			return "Celsius must be Decimal";
		if(Double.parseDouble(c) < -273.15 || Double.parseDouble(c) >100)
			return "Value of Celsius must be -273.15 to 100";
		if(c.length() > 10)
			return "Exeed value";
		return "Valid";
	}
	public String validateFahrenheit(Temp t) {
		// TODO Auto-generated method stub
		String c = t.getF();
		
		if(c.equals("")) 
			return "Fahrenheit must not be empty";
		if(c == null)
			return "Fahrenheit must not be empty";
		if(!isNumbric(c))
			return "Fahrenheit must be Decimal";
		if(Double.parseDouble(c) < -459.67 || Double.parseDouble(c) > 212.00)
			return "Value of Fahrenheit must be -459.67 to 212";
		if(c.length() > 10)
			return "Exeed value";	
		return "Valid";
	}

	private boolean isNumbric(String c) {
		// TODO Auto-generated method stub
		boolean result=true;
		try {
			Double.parseDouble(c);
		}catch(Exception e) {
			result=false;
		}
		return result;
	}

}
