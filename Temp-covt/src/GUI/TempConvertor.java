package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import controller.Convetor;
import data.Temp;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.lang.ModuleLayer.Controller;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.ComponentOrientation;
import javax.swing.border.TitledBorder;

public class TempConvertor {

	private JFrame frmTempratureConvetor;
	private JTextField tfC;
	private JTextField tfF;
	private Controller controler1;
	private Temp temp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TempConvertor window = new TempConvertor();
					window.frmTempratureConvetor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TempConvertor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTempratureConvetor = new JFrame();
		frmTempratureConvetor.setBackground(new Color(0, 51, 0));
		frmTempratureConvetor.setForeground(new Color(0, 204, 0));
		frmTempratureConvetor.setTitle("Temperature Convetor");
		frmTempratureConvetor.getContentPane().setBackground(new Color(0, 51, 0));
		frmTempratureConvetor.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 11, 414, 239);
		frmTempratureConvetor.getContentPane().add(panel);
		panel.setLayout(null);
		
		tfC = new JTextField();
		tfC.setForeground(new Color(0, 0, 0));
		tfC.setBounds(10, 50, 152, 52);
		panel.add(tfC);
		tfC.setBackground(new Color(255, 255, 255));
		tfC.setFont(new Font("Times New Roman", Font.BOLD, 24));
		tfC.setBorder(new TitledBorder(new MatteBorder(1, 5, 3, 3, (Color) new Color(0, 0, 0)), "Celsius", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		tfC.setColumns(10);
		
		JButton btnNewButton = new JButton("Celsius To Fahreheit");
		btnNewButton.setBorder(new MatteBorder(1, 2, 3, 2, (Color) new Color(153, 153, 153)));
		btnNewButton.setForeground(new Color(204, 204, 204));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.setBounds(20, 113, 152, 47);
		panel.add(btnNewButton);
		btnNewButton.setBackground(new Color(0, 0, 0));
		
		JButton btnFarenheitToCelsius = new JButton("Fahreheit To Celsius");
		btnFarenheitToCelsius.setBorder(new MatteBorder(1, 2, 3, 2, (Color) new Color(153, 153, 153)));
		btnFarenheitToCelsius.setForeground(new Color(204, 204, 204));
		btnFarenheitToCelsius.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnFarenheitToCelsius.setBounds(252, 113, 152, 47);
		panel.add(btnFarenheitToCelsius);
		btnFarenheitToCelsius.setBackground(new Color(0, 0, 0));
		
		tfF = new JTextField();
		tfF.setForeground(new Color(0, 0, 0));
		tfF.setFocusCycleRoot(true);
		tfF.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		tfF.setAlignmentX(Component.RIGHT_ALIGNMENT);
		tfF.setBounds(252, 50, 152, 52);
		panel.add(tfF);
		tfF.setFont(new Font("Times New Roman", Font.BOLD, 24));
		tfF.setColumns(10);
		tfF.setBorder(new TitledBorder(new MatteBorder(1, 5, 4, 3, (Color) new Color(0, 0, 0)), "Fahrenheit", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		tfF.setBackground(new Color(255, 255, 255));
		
				JLabel lblNewLabel_1 = new JLabel("*");
				lblNewLabel_1.setBounds(10, 188, 400, 26);
				panel.add(lblNewLabel_1);
				lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
				
				JPanel panel_1 = new JPanel();
				panel_1.setBackground(new Color(0, 0, 0));
				panel_1.setBounds(0, 0, 414, 39);
				panel.add(panel_1);
				
				JLabel lblTemprature = new JLabel("Temperature Convetor");
				lblTemprature.setFont(new Font("Algerian", Font.BOLD, 24));
				lblTemprature.setForeground(new Color(255, 255, 255));
				panel_1.add(lblTemprature);
		btnFarenheitToCelsius.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Convetor c  =new Convetor();
				Temp t = new Temp(tfF.getText(),tfC.getText());
				String ans=c.calculateCelsius(t, tfF.getText());
				if(ans.equals("Valid")) {
					lblNewLabel_1.setText("");
					tfC.setText(t.getC());
				}
				else {

					lblNewLabel_1.setText(ans);
					lblNewLabel_1.setForeground(Color.RED);
				}
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Convetor c  =new Convetor();
				Temp t = new Temp(tfF.getText(),tfC.getText());
				String ans=c.calculateFaHreHeit(t, tfC.getText());
				if(ans.equals("Valid")) {
					lblNewLabel_1.setText("");
					tfF.setText(t.getF());
				}
				else {

					lblNewLabel_1.setText(ans);
					lblNewLabel_1.setForeground(Color.RED);
				}
					
			}
		});
		
		frmTempratureConvetor.setBounds(100, 100, 450, 300);
		frmTempratureConvetor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
