package controller;

import java.text.DecimalFormat;

import data.Temp;
import validate.Validation;

public class Convetor {
	private Temp t;
	public String calculateCelsius(Temp t,String f) {
		t.setF(f);
		String ans="";
		Validation validation = new Validation();
		ans = validation.validateFahrenheit(t);
		if(ans.equals("Valid")) {
			double f1 = Double.parseDouble(t.getF());
			double c = (double)((f1 -32)*(5))/9;
			c =Double.parseDouble(new DecimalFormat("##.##").format(c));
			t.setC(c+"");
			return ans;
		}
		else
			return ans;
		
	}
	public String calculateFaHreHeit(Temp t,String c) {
		t.setC(c);
		String ans="";
		Validation validation = new Validation();
		ans = validation.validateCelsius(t);
		if(ans.equals("Valid")) {
			double c1 = Double.parseDouble(t.getC());
			double f = ((double)(c1 *9)/5) +32.0;
			f =Double.parseDouble(new DecimalFormat("##.##").format(f));
			t.setF(f+"");
			return ans;
		}
		else
			return ans;
		
	}
}
