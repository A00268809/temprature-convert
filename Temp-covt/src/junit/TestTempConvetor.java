package junit;

import controller.Convetor;
import data.Temp;
import junit.framework.TestCase;

public class TestTempConvetor extends TestCase {
	
	 /*

    Test Number: 1

    Test Objective: empty Celsius and empty Fahrenhiet

    Test Type: jUnit

    Input(s): celsius="";Fahrenheit ="";

    Expected Output: Celsius must not be empty
    				Fahrenheit must not be empty

    */
	public void testTempTest001() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Celsius must not be empty",c.calculateFaHreHeit(t, ""));
		assertEquals("Fahrenheit must not be empty",c.calculateCelsius(t, ""));
	}
	 /*

    Test Number: 2

    Test Objective: invalid min -1

    Test Type: jUnit

    Input(s): celsius=-273.16; Fahrenheit =459.68;

    Expected Output: Value of Fahrenheit must be -459.67 to 212
    					Value of Celsius must be -273.15 to 100

    */
	public void testTempTest002() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Value of Celsius must be -273.15 to 100",c.calculateFaHreHeit(t, "-273.16"));
		assertEquals("Value of Fahrenheit must be -459.67 to 212",c.calculateCelsius(t, "-459.68"));
	}
	
	 /*

    Test Number: 3

    Test Objective: valid  min 

    Test Type: jUnit

    Input(s): celsius=-273.15;Fahrenheit=-459.67;

    Expected Output: valid
    				valid

    */
	public void testTempTest003() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "-273.15"));
		assertEquals("Valid",c.calculateCelsius(t, "-459.67"));
	}
	 /*

    Test Number: 4

    Test Objective: valid min +1

    Test Type: jUnit

    Input(s): celsius=-273.14;Fahrenheit= 459.66

    Expected Output: valid
    				valid

    */
	public void testTempTest004() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "-273.14"));
		assertEquals("Valid",c.calculateCelsius(t, "-459.66"));
	}
	 /*

    Test Number: 5

    Test Objective: valid mid

    Test Type: jUnit

    Input(s): celsius=0.14;Fahrenheit=123.34;

    Expected Output: valid
    			valid

    */
	public void testTempTest005() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "0.14"));
		assertEquals("Valid",c.calculateCelsius(t, "123.34"));
	}

	 /*

    Test Number: 6

    Test Objective: valid max -1

    Test Type: jUnit

    Input(s): celsius=99.99;Fahrenheit=211.99

    Expected Output: valid
    				Valid

    */
	public void testTempTest006() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "99.99"));
		assertEquals("Valid",c.calculateCelsius(t, "211.9"));
	}

	 /*

    Test Number: 7

    Test Objective: valid max

    Test Type: jUnit

    Input(s): celsius=100;Fahrenheit=212

    Expected Output: valid
    				Valid

    */
	public void testTempTest007() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "100"));
		assertEquals("Valid",c.calculateCelsius(t, "212"));
		
	}
	 /*

    Test Number: 8

    Test Objective: In-valid max+1

    Test Type: jUnit

    Input(s): celsius=100.01;Fahrenheit=212;

    Expected Output: Value of Celsius must be -273.15 to 100
    					Value of Fahrenheit must be -459.67 to 212

    */
	public void testTempTest008() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Value of Celsius must be -273.15 to 100",c.calculateFaHreHeit(t, "100.01"));
		assertEquals("Value of Fahrenheit must be -459.67 to 212",c.calculateCelsius(t, "212.01"));
	}

	/*

    Test Number: 9

    Test Objective: In-valid String value

    Test Type: jUnit

    Input(s): celsius="String";Fahrenhe="String";

    Expected Output: Celsius must be Decimal
    				Fahrenheit must be Decimal

    */
	public void testTempTest009() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Celsius must be Decimal",c.calculateFaHreHeit(t, "String"));
		assertEquals("Fahrenheit must be Decimal",c.calculateCelsius(t, "String"));
	}

	/*

    Test Number:10

    Test Objective: In-valid length+1

    Test Type: jUnit

    Input(s): celsius= 23.34534571;Fahrenhiet== 23.34534571;

    Expected Output: Exeed value
    				Exeed value

    */
	public void testTempTest010() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Exeed value",c.calculateFaHreHeit(t, "23.34534571"));
		assertEquals("Exeed value",c.calculateCelsius(t, "23.34534571"));
	}
	/*

    Test Number:11

    Test Objective: valid length

    Test Type: jUnit

    Input(s): celsius= 23.3453457;Fahrenheit=23.3453457;

    Expected Output: Valid

    */
	public void testTempTest011() {
		Convetor c  =new Convetor();
		Temp t = new Temp("","");
		assertEquals("Valid",c.calculateFaHreHeit(t, "23.3453457"));
		assertEquals("Valid",c.calculateCelsius(t, "23.3453457"));
		}	
}
